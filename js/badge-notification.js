(function ($, Drupal) {
  'use strict';
  
  Drupal.behaviors.badge_notification = {
    attach: function (context, settings) {
      var $selector = $('.badge-notification-placeholder', context);
      
      if (!$selector.length) {
        return false;
      }
      
      var badges = {};
      
      $selector.each(function () {
        var id = $(this).data('id');
        badges[id] = {
          "id": id,
          "plugin-name": $(this).data('plugin-name'),
          "attributes": $(this).data('attributes'),
        };
      });
      
      if (badges) {
        $.ajax({
          type: 'POST',
          url: Drupal.url('badge-notification/get/json'),
          data: JSON.stringify({"badges": badges}),
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          success: function (data) {
            if (!data.badges) {
              return false;
            }
            
            $.each(data.badges, function (id, markup) {
              $('.badge-notification-placeholder[data-id="'+ id +'"]').html(markup);
            });
          },
          error: function (error) {
          
          }
        });
      }
    }
  };
  
}(jQuery, Drupal));