<?php

namespace Drupal\badge_notification\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Badge notification item annotation object.
 *
 * @see \Drupal\badge_notification\Plugin\BadgesAsyncManager
 * @see plugin_api
 *
 * @Annotation
 */
class BadgeNotification extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Define if plugin has menu notification.
   *
   * @var bool
   */
  public bool $has_menu_notification = FALSE;

}
