<?php

namespace Drupal\badge_notification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Admin form to configure badge notification.
 */
class BadgeNotificationSettingsForm extends ConfigFormBase {

  /**
   * Module config alias.
   *
   * @var string
   */
  const SETTINGS = 'badge_notification.settings';

  /**
   * Minimum days settings.
   *
   * @var int
   */
  const DAYS_LIMIT_MIN = 1;

  /**
   * Maximum days settings.
   *
   * @var int
   */
  const DAYS_LIMIT_MAX = 30;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'badge_notification_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['badge_notification_days_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum content age to display status (days)'),
      '#default_value' => $config->get('days_limit') ?: 7,
      '#attributes' => ['min' => 1, 'max' => 30],
    ];

    $form['badge_notification_status_display'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Display content status'),
      '#options' => [
        'new' => $this->t('New content'),
        'updated' => $this->t('Updated content'),
      ],
      '#default_value' => $config->get('status_display') ?: [
        'new' => 'new',
        'updated' => 0,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $days_limit = $form_state->getValue('badge_notification_days_limit');

    if (!is_numeric($days_limit) || $days_limit < static::DAYS_LIMIT_MIN || $days_limit > static::DAYS_LIMIT_MAX) {
      $form_state->setErrorByName('badge_notification_days_limit', $this->t('Please enter a valid number'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('days_limit', $form_state->getValue('badge_notification_days_limit'))
      ->set('status_display', $form_state->getValue('badge_notification_status_display'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
