<?php

namespace Drupal\badge_notification\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Badge notification plugins.
 */
interface BadgeNotificationInterface extends PluginInspectionInterface {

  /**
   * Return the content of the badge result.
   *
   * @return string
   *   The string result of the badge plugin.
   */
  public function badgeResult(string $badge_id, string $attributes): string;

}
