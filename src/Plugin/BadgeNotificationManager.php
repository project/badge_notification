<?php

namespace Drupal\badge_notification\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Badge notification plugin manager.
 */
class BadgeNotificationManager extends DefaultPluginManager {

  /**
   * Constructs a new BadgeNotificationManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BadgeNotification', $namespaces, $module_handler, 'Drupal\badge_notification\Plugin\BadgeNotificationInterface', 'Drupal\badge_notification\Annotation\BadgeNotification');

    $this->alterInfo('badge_notification_badge_notification_info');
    $this->setCacheBackend($cache_backend, 'badge_notification_badge_notification_plugins');
  }

}
