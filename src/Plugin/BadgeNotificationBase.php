<?php

namespace Drupal\badge_notification\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for Badge notification plugins.
 */
abstract class BadgeNotificationBase extends PluginBase implements BadgeNotificationInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function badgeResult(string $badge_id, string $attributes): string {
    return $this->t('unknown badge handler : @badge_id', ['@badge_id' => $badge_id], ['context' => 'Badge notification']);
  }

}
