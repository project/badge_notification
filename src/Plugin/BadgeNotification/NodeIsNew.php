<?php

namespace Drupal\badge_notification\Plugin\BadgeNotification;

use Drupal\badge_notification\Plugin\BadgeNotificationBase;
use Drupal\badge_notification\Service\BadgeNotificationCore;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'node_is_new' badge notification.
 *
 * @BadgeNotification(
 *   id = "node_is_new",
 *   label = @Translation("Node is new")
 * )
 */
class NodeIsNew extends BadgeNotificationBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The badge notification core.
   *
   * @var \Drupal\badge_notification\Service\BadgeNotificationCore
   */
  protected $badgeNotificationCore;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\badge_notification\Service\BadgeNotificationCore $badgeNotificationCore
   *   The badge notification core helper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Connection $database,
    AccountProxyInterface $currentUser,
    EntityTypeManagerInterface $entityTypeManager,
    BadgeNotificationCore $badgeNotificationCore
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;

    $this->badgeNotificationCore = $badgeNotificationCore;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('badge_notification.core')
    );
  }

  /**
   * Return badge content output.
   *
   * @param string $badge_id
   *   Badge unique id.
   * @param string $attributes
   *   Badge attributes.
   *
   * @return string
   *   Return rendered badge content.
   */
  public function badgeResult(string $badge_id, string $attributes): string {
    if (!$status = $this->getNodeStatus($attributes)) {
      return '';
    }

    return $status;
  }

  /**
   * Get node status.
   *
   * @param int $nid
   *   Node id to check status.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   Return node status string.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getNodeStatus(int $nid): string {
    /** @var \Drupal\node\Entity\Node $node */
    if (!$node = $this->entityTypeManager->getStorage('node')->load($nid)) {
      return '';
    }

    $status_display = $this->badgeNotificationCore->getStatusDisplay();
    $time_limit = $this->badgeNotificationCore->getTimeLimit();
    $nid = $node->id();
    $nodeLastViewed = $this->getNodeLastViewed($nid);

    if ($status_display['new'] && $nodeLastViewed == 0 && $node->getCreatedTime() > $time_limit) {
      return $this->t('New', [], ['context' => 'Badge notification']);
    }
    elseif ($status_display['updated'] && $node->getChangedTime() > $nodeLastViewed && $node->getChangedTime() > $time_limit) {
      return $this->t('Updated', [], ['context' => 'Badge notification']);
    }

    return '';
  }

  /**
   * Get node last viewed timestamp.
   *
   * @param int $nid
   *   Node id to check status.
   *
   * @return int
   *   Return node last viewed timestamp.
   */
  protected function getNodeLastViewed($nid): int {
    if ($nodeLastViewed = &drupal_static(__METHOD__)) {
      return $nodeLastViewed;
    }

    $query = $this->database->query("SELECT timestamp FROM {history} WHERE uid = :uid AND nid = :nid", [
      ':uid' => $this->currentUser->id(),
      ':nid' => $nid,
    ]);

    $history = $query->fetchObject();
    return $history->timestamp ?? 0;
  }

}
