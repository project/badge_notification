<?php

namespace Drupal\badge_notification\Plugin\BadgeNotification;

use Drupal\badge_notification\Plugin\BadgeNotificationBase;
use Drupal\badge_notification\Service\BadgeNotificationMenu;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'views_has_new' badge notification.
 *
 * @BadgeNotification(
 *   id = "views_has_new",
 *   label = @Translation("Views has new"),
 *   has_menu_notification = true
 * )
 */
class ViewsHasNew extends BadgeNotificationBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The badge notification Menu service.
   *
   * @var \Drupal\badge_notification\Service\BadgeNotificationMenu
   */
  protected $badgeNotificationMenu;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\badge_notification\Service\BadgeNotificationMenu $badgeNotificationMenu
   *   The badge notification core helper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $configFactory,
    Connection $database,
    AccountProxyInterface $currentUser,
    EntityTypeManagerInterface $entityTypeManager,
    BadgeNotificationMenu $badgeNotificationMenu
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->database = $database;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->badgeNotificationMenu = $badgeNotificationMenu;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('badge_notification.menu')
    );
  }

  /**
   * Return badge content output.
   *
   * @param string $badge_id
   *   Badge unique id.
   * @param string $attributes
   *   Badge attributes.
   *
   * @return string
   *   Return rendered badge content.
   */
  public function badgeResult(string $badge_id, string $attributes): string {
    $count = $this->badgeNotificationMenu->viewsCountNew($attributes);
    if (empty($count)) {
      return '';
    }

    return $this->t('New', [], ['context' => 'Badge notification']);
  }

}
