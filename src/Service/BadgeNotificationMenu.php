<?php

namespace Drupal\badge_notification\Service;

use Drupal\badge_notification\Plugin\BadgeNotificationManager;
use Drupal\views\Views;

/**
 * BadgeNotificationMenu.
 *
 * Service to handle menu link badge notification.
 * Based on settings from menu link.
 */
class BadgeNotificationMenu {

  /**
   * The badge notification manager.
   *
   * @var \Drupal\badge_notification\Plugin\BadgeNotificationManager
   */
  protected $badgeNotificationManager;

  /**
   * Constructor.
   *
   * @param \Drupal\badge_notification\Plugin\BadgeNotificationManager $badgeNotificationManager
   *   The badge notification manager.
   */
  public function __construct(BadgeNotificationManager $badgeNotificationManager) {
    $this->badgeNotificationManager = $badgeNotificationManager;
  }

  /**
   * Get handlers options.
   *
   * @return array
   *   Return an array of options.
   */
  public function getHandlersOptions(): array {
    $options = ['' => t('None')];

    $definitions = $this->badgeNotificationManager->getDefinitions();
    $views_options = $this->getHandlersViewsOptions();

    foreach ($definitions as $definition) {

      if (empty($definition['has_menu_notification'])) {
        continue;
      }

      /** @var \Drupal\Core\StringTranslation\TranslatableMarkup $group */
      $group = $definition['label'];
      $group_label = $group->render();

      foreach ($views_options as $key => $view_option) {
        $group_key = implode('::', [$definition['id'], $key]);
        $options[$group_label][$group_key] = $view_option;
      }
    }

    return $options;
  }

  /**
   * Get handlers views options.
   *
   * @return array
   *   Return an array of views options.
   */
  public function getHandlersViewsOptions(): array {
    $options = [];
    $views = Views::getEnabledViews();

    foreach ($views as $view) {
      if ($view->get('base_table') !== 'node_field_data') {
        continue;
      }

      $options[$view->id()] = $view->label();
      $displays = $view->get('display');

      foreach ($displays as $display) {
        if ($display['id'] === 'default') {
          continue;
        }

        $key = implode('::', [$view->id(), $display['id']]);
        $options[$key] = '-- ' . $display['display_title'];
      }
    }

    return $options;
  }

  /**
   * Render Menu Link Content's placeholder attributes.
   *
   * @param string $badge_notification_option
   *   Badge notification options merged into a string with "::" separator.
   *
   * @return array
   *   Return an array of attributes for the badge placeholder.
   */
  public function renderMenuLinkcontentPlaceholderAttributes(string $badge_notification_option): array {
    $option_array = explode('::', $badge_notification_option);
    $plugin_name = array_shift($option_array);

    return [
      'class' => 'badge-notification-placeholder',
      'data-id' => uniqid('badge-'),
      'data-plugin-name' => $plugin_name,
      'data-attributes' => implode('/', $option_array),
    ];
  }

  /**
   * Get the view based on plugin attributes.
   *
   * @param string $attributes
   *   Array of plugin attributes to grab the view (id, display, arguments)
   *
   * @return \Drupal\views\ViewExecutable|false
   *   Return the view or false if not found.
   */
  public function getView(string $attributes) {
    $view_setup = $this->resolvePluginAttributes($attributes);

    if (empty($view_setup['id'])) {
      return FALSE;
    }

    $view_id = $view_setup['id'];
    $view_display = $view_setup['display'] ?: 'default';
    $view_arguments = $view_setup['arguments'] ?: [];

    $view = Views::getView($view_id);
    $view->views_count_new = TRUE;
    $view->setDisplay($view_display);
    $view->setArguments($view_arguments);
    $view->execute();

    return $view;
  }

  /**
   * Resolve plugin string attributes.
   *
   * @param string $attributes
   *   Merged string with plugin definition splited with "/" char.
   *
   * @return array
   *   Array of plugin attributes with id, display and arguments.
   */
  public function resolvePluginAttributes(string $attributes): array {

    $attributes_array = explode('/', $attributes);

    return [
      'id' => array_shift($attributes_array),
      'display' => array_shift($attributes_array),
      'arguments' => $attributes_array,
    ];

  }

  /**
   * Count views total rows.
   *
   * @param string $attributes
   *   Badge attributes.
   *
   * @return int
   *   result new result count.
   */
  public function viewsCountNew(string $attributes): int {

    $view = $this->getView($attributes);

    if (!$view) {
      return 0;
    }

    return $view->total_rows;
  }

}
