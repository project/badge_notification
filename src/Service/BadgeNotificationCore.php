<?php

namespace Drupal\badge_notification\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * BadgeNotification Core class.
 *
 * Helper functions to get module settings.
 */
class BadgeNotificationCore {

  /**
   * Configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Datetime.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $datetime;

  /**
   * Status display configuration.
   *
   * @var mixed
   */
  protected $statusDisplay;

  /**
   * Date limit configuration.
   *
   * @var int
   */
  protected $timeLimit;

  /**
   * Constructs a BadgeNotification Core object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Component\Datetime\TimeInterface $datetime
   *   The datetime.
   */
  public function __construct(ConfigFactoryInterface $configFactory, TimeInterface $datetime) {
    $this->configFactory = $configFactory;
    $this->datetime = $datetime;

    $config = $this->configFactory->get('badge_notification.settings');
    $this->statusDisplay = $config->get('status_display') ?: [
      'new' => 'new',
      'updated' => 0,
    ];
    $days_limit = $config->get('days_limit') ?: 7;
    $this->timeLimit = strtotime('-' . $days_limit . ' day', $this->datetime->getRequestTime());
  }

  /**
   * Return status display settings.
   *
   * @return array
   *   configuration array for all status display.
   */
  public function getStatusDisplay(): array {
    return $this->statusDisplay;
  }

  /**
   * Return time limit settings.
   *
   * @return int
   *   time limit in unix timestamp.
   */
  public function getTimeLimit() {
    return $this->timeLimit;
  }

}
