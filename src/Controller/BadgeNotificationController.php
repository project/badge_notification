<?php

namespace Drupal\badge_notification\Controller;

use Drupal\badge_notification\Plugin\BadgeNotificationManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * BadgesAsync controller.
 */
class BadgeNotificationController extends ControllerBase {

  /**
   * The badge notification manager.
   *
   * @var \Drupal\badge_notification\Plugin\BadgeNotificationManager
   */
  protected BadgeNotificationManager $badgeNotificationManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Constructor.
   *
   * @param \Drupal\badge_notification\Plugin\BadgeNotificationManager $badgeNotificationManager
   *   The badge notification manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(BadgeNotificationManager $badgeNotificationManager,
                              EntityTypeManagerInterface $entityTypeManager,
                              RendererInterface $renderer) {
    $this->badgeNotificationManager = $badgeNotificationManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.badge_notification'),
      $container->get('entity_type.manager'),
      $container->get('renderer'),
    );
  }

  /**
   * Returns json response.
   *
   * All badges request are aggregated with their plugin name and attributes.
   * The JSON response returns the rendered badge for each element.
   */
  public function json(Request $request): JsonResponse {
    $content = json_decode($request->getContent(), TRUE);
    $plugins = [];
    $badges = [];

    // Fetch badge plugins.
    foreach ($content['badges'] as $id => $badge) {
      if (empty($badge['plugin-name'])) {
        continue;
      }
      elseif (!empty($plugins[$badge['plugin-name']])) {
        /** @var \Drupal\badge_notification\Plugin\BadgeNotificationBase $plugin */
        $plugin = $plugins[$badge['plugin-name']];
      }
      else {
        /** @var \Drupal\badge_notification\Plugin\BadgeNotificationBase $plugin */
        $plugin = $this->badgeNotificationManager->createInstance($badge['plugin-name']);
        $plugins[$badge['plugin-name']] = $plugin;
      }

      $badge_result = $plugin->badgeResult($id, $badge['attributes']);

      if ($badge_result) {
        $element = [
          '#theme' => 'badge_notification',
          '#content' => $badge_result,
        ];

        $badges[$id] = $this->renderer->render($element);
      }

    }

    return new JsonResponse([
      'badges' => $badges,
    ]);
  }

}
